#!/usr/bin/env bash

sed -i 's/101/0/g' /usr/sbin/policy-rc.d
apt-get update
apt-get install -y software-properties-common && \
add-apt-repository ppa:ondrej/php -y && \
apt-get update

apt-get install -y \
     php5.6-fpm \
     php5.6-curl \
     php5.6-gd \
     php5.6-mcrypt \
     php5.6-mysql \
     php5.6-xsl \
     php-xdebug \
     php-memcache \
     php-redis \
     php-imagick \
     php5.6-dev \
     pkg-config \
     php5.6-cli \
     php-pear \
     php5.6-common \
     php5.6-bz2 \
     php5.6-mbstring \
     php5.6-zip \
     git

/usr/bin/pecl install mongo

echo "extension=mongo.so" > /etc/php/5.6/mods-available/mongo.ini
ln -s /etc/php/5.6/mods-available/mongo.ini /etc/php/5.6/fpm/conf.d/20-mongo.ini
ln -s /etc/php/5.6/mods-available/mongo.ini /etc/php/5.6/cli/conf.d/20-mongo.ini

sed -i 's/listen = \/run\/php\/php5.6-fpm.sock/listen = 9000/g' /etc/php/5.6/fpm/pool.d/www.conf
sed -i '/^error_log =/c\error_log = /proc/self/fd/2' /etc/php/5.6/fpm/php-fpm.conf
sed -i \
    -e '/^;catch_workers_output/ccatch_workers_output = yes' \
    -e '/^pm.max_children =/c\pm.max_children = 40' \
    -e '/^pm.start_servers =/c\pm.start_servers = 10' \
    -e '/^pm.min_spare_servers =/c\pm.min_spare_servers = 10' \
    -e '/^pm.max_spare_servers =/c\pm.max_spare_servers = 20' \
    -e '/^;pm.max_requests =/c\pm.max_requests = 500' /etc/php/5.6/fpm/pool.d/www.conf
sed -i \
    -e '/^display_errors =/c\display_errors = On' \
    -e '/^display_startup_errors =/c\display_startup_errors = On' \
    -e '/^;date.timezone =/c\date.timezone = UTC' \
    -e '/^upload_max_filesize =/c\upload_max_filesize = 8m' \
    -e '/^error_reporting =/c\error_reporting = E_ALL' /etc/php/5.6/fpm/php.ini /etc/php/5.6/cli/php.ini

rm /etc/php/5.6/mods-available/opcache.ini

apt-get autoclean
apt-get clean
du -sh /var/cache/apt/archives
apt-get autoremove

