#!/usr/bin/env bash

apt-get autoclean
apt-get clean
du -sh /var/cache/apt/archives
apt-get autoremove